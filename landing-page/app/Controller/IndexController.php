<?php

namespace App\Controller;

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
	public function index()
	{
		$view = new \Phalcon\Mvc\View\Simple();
    	$view->setViewsDir('views/');
    	echo $view->render('index');
	}
}