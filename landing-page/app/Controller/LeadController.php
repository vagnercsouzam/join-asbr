<?php

namespace App\Controller;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use App\Service\Lead as LeadService;
use App\Entity\FirstStep as FirstStepEntity;
use App\Entity\SecondStep as SecondStepEntity;
use App\Exception\Landing as LandingException;

class LeadController extends Controller
{
	public function firstStep()
	{
		$response = new Response();
		$leadService = new LeadService();
		$firstStepEntity = new FirstStepEntity();

		$firstStepEntity = $firstStepEntity->exchangeArray(
			$this->request->getPost()
		);

		try {
			$leadService->validateFirstStep($firstStepEntity);

			$response->setContent(
	    		json_encode(
	    			[
	    				'valid' => true
	    			]
	    		)
	    	);
		} catch (LandingException $e) {
			$response->setContent(
	    		json_encode(
	    			[
	    				'valid' => false,
	    				'message' => $e->getMessage()
	    			]
	    		)
	    	);
		} catch (\Exception $e) {
			error_log($e->getMessage());

	    	$response->setContent(
	    		json_encode(
	    			[
	    				'valid' => false,
	    				'message' => 'Ocorreu um problema inesperado. Tente mais tarde.'
	    			]
	    		)
	    	);
		} finally {
			$response->setContentType('application/json');

			return $response;
		}
	}

	public function secondStep()
	{
		$response = new Response();
		$leadService = new LeadService();
		$secondStepEntity = new SecondStepEntity();

		$secondStepEntity = $secondStepEntity->exchangeArray(
			$this->request->getPost()
		);

		try {
			$leadService->validateSecondStep($secondStepEntity);

			$secondStepEntity->setScore(
				$leadService->calculateScore($secondStepEntity)
			);

			$leadService->saveLead($secondStepEntity);

			$response->setContent(
	    		json_encode(
	    			[
	    				'success' => true
	    			]
	    		)
	    	);
		} catch (LandingException $e) {
			$response->setContent(
	    		json_encode(
	    			[
	    				'success' => false,
	    				'message' => $e->getMessage()
	    			]
	    		)
	    	);
		} catch (\Exception $e) {
			error_log($e->getMessage());

	    	$response->setContent(
	    		json_encode(
	    			[
	    				'success' => false,
	    				'message' => 'Ocorreu um problema inesperado. Tente mais tarde.'
	    			]
	    		)
	    	);
		} finally {
			$response->setContentType('application/json');

			return $response;
		}
	}

	public function units()
	{
		$response = new Response();
		$leadService = new LeadService();

		try {
			$units = $leadService->getUnitsByRegion(
				$this->request->get('region')
			);

			$response->setContent(
	    		json_encode(
	    			[
	    				'success' => true,
	    				'units' => $units
	    			]
	    		)
	    	);
		} catch (LandingException $e) {
			$response->setContent(
	    		json_encode(
	    			[
	    				'success' => false,
	    				'message' => $e->getMessage()
	    			]
	    		)
	    	);
		} catch (\Exception $e) {
			error_log($e->getMessage());

	    	$response->setContent(
	    		json_encode(
	    			[
	    				'success' => false,
	    				'message' => 'Ocorreu um problema inesperado. Tente mais tarde.'
	    			]
	    		)
	    	);
		} finally {
			$response->setContentType('application/json');

			return $response;
		}
	}
}