<?php

namespace App\Entity;

class FirstStep
{
	private $name;
	private $birthday;
	private $mail;
	private $phone;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = trim($name);

        return $this;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = trim($birthday);

        return $this;
    }

    public function getMail()
    {
        return $this->mail;
    }

    public function setMail($mail)
    {
        $this->mail = trim($mail);

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = trim($phone);

        return $this;
    }

    public function exchangeArray($data)
    {
    	$this->setName(empty($data['name']) ? null : $data['name']);
    	$this->setBirthday(empty($data['birthday']) ? null : $data['birthday']);
    	$this->setMail(empty($data['mail']) ? null : $data['mail']);
    	$this->setPhone(empty($data['phone']) ? null : $data['phone']);

    	return $this;
    }
}