<?php

namespace App\Entity;

class SecondStep extends FirstStep
{
	private $region;
	private $unit;
    private $score = 10;

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->setRegion(empty($data['region']) ? null : $data['region']);
        $this->setUnit(empty($data['unit']) ? null : $data['unit']);

        return $this;
    }
}