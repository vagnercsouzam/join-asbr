<?php

namespace App\Sdk;

use App\Entity\SecondStep as SecondStepEntity;
use GuzzleHttp\Client;

class ActualSales
{
	const API_TOKEN = '11cbfabb2ab8af75e168d4416ab68083';

	public function saveLead(SecondStepEntity $secondStepEntity)
	{
		try {
			$response = $this->getClient()
				->post(
					'/join-asbr/ti/lead',
					[
						'form_params' => [
							'nome' => $secondStepEntity->getName(),
							'email' => $secondStepEntity->getMail(),
							'telefone' => $secondStepEntity->getPhone(),
							'regiao' => $secondStepEntity->getRegion(),
							'unidade' => $secondStepEntity->getUnit(),
							'data_nascimento' => $this->getDateFormatted($secondStepEntity->getBirthday()),
							'score' => (int) $secondStepEntity->getScore(),
							'token' => self::API_TOKEN
						]
					]
				);

			if ($response->getStatusCode() == 200) {
				return true;
			}
		} catch (\Exception $e) {
			error_log($e->getMessage());
		}

		return false;
	}

	private function getDateFormatted($date)
	{
		return \DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d');
	}

	private function getClient()
	{
		return new Client([
		    'base_uri' => 'http://api.actualsales.com.br'
		]);
	}
}