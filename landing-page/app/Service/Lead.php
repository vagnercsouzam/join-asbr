<?php

namespace App\Service;

use App\Entity\FirstStep as FirstStepEntity;
use App\Entity\SecondStep as SecondStepEntity;

class Lead
{
	public function validateFirstStep(FirstStepEntity $firstStep)
	{
		(new \App\Validator\Name())->validate($firstStep->getName());
		(new \App\Validator\Birthday())->validate($firstStep->getBirthday());
		(new \App\Validator\Mail())->validate($firstStep->getMail());
		(new \App\Validator\Phone())->validate($firstStep->getPhone());
	}

	public function validateSecondStep(SecondStepEntity $secondStep)
	{
		(new \App\Validator\Region())->validate($secondStep->getRegion());

		if ($secondStep->getRegion() != 'Norte') {
			(new \App\Validator\Unit())->validate($secondStep->getUnit());
		}
	}

	public function calculateScore(SecondStepEntity $secondStep)
	{
		return (new \App\Service\Score\Calculator())->calculate($secondStep);
	}

	public function saveLead(SecondStepEntity $secondStep)
	{
		return (new \App\Sdk\ActualSales())->saveLead($secondStep);
	}

	public function getUnitsByRegion($region)
	{
		$regions = [
			'Sul' => [
				'Porto Alegre',
				'Curitiba'
			],
			'Sudeste' => [
				'São Paulo',
				'Rio de Janeiro',
				'Belo Horizonte'
			],
			'Centro-Oeste' => [
				'Brasília'
			],
			'Nordeste' => [
				'Salvador',
				'Recife'
			],
			'Norte' => []
		];

		return isset($regions[$region]) ? $regions[$region] : [];
	}
}