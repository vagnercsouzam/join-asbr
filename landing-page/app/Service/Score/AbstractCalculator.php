<?php

namespace App\Service\Score;

use App\Entity\SecondStep as SecondStepEntity;

abstract class AbstractCalculator
{
	private $nextCalculator;

	public abstract function calculate(SecondStepEntity $secondStep);

	public function setNext(AbstractCalculator $nextCalculator)
	{
		if ($this->nextCalculator == null) {
			$this->nextCalculator = $nextCalculator;
		} else {
			$this->nextCalculator
				->setNext($nextCalculator);
		}
	}

	public function calculateNext(SecondStepEntity $secondStep)
	{
		if ($this->nextCalculator != null) {
			return $this->nextCalculator
				->calculate($secondStep);
		}

		return $secondStep->getScore();
	}
}