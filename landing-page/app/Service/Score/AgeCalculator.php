<?php

namespace App\Service\Score;

use App\Entity\SecondStep as SecondStepEntity;

class AgeCalculator extends AbstractCalculator
{
	public function calculate(SecondStepEntity $secondStep)
	{
		$age = $this->getAge($secondStep);

		if ($age < 18 || $age >= 100) {
			$secondStep->setScore(
				$secondStep->getScore() - 5
			);
		} else if ($age >= 40) {
			$secondStep->setScore(
				$secondStep->getScore() - 3
			);
		}

		return $this->calculateNext($secondStep);
	}

	private function getAge($secondStep)
	{
		$now = new \DateTime('2016-06-01');
		$birthday = \DateTime::createFromFormat('d/m/Y', $secondStep->getBirthday());
		$diff = $birthday->diff($now);
		return $diff->y;
	}
}