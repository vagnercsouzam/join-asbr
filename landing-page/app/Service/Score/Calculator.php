<?php

namespace App\Service\Score;

use App\Entity\SecondStep as SecondStepEntity;

class Calculator
{
	public function calculate(SecondStepEntity $secondStep)
	{
		$calculator = new RegionCalculator();
		$calculator->setNext(new AgeCalculator());

		return $calculator->calculate($secondStep);
	}
}