<?php

namespace App\Service\Score;

use App\Entity\SecondStep as SecondStepEntity;

class RegionCalculator extends AbstractCalculator
{
	public function calculate(SecondStepEntity $secondStep)
	{
		if ($secondStep->getUnit() != 'São Paulo') {
			$secondStep->setScore(
				$this->getScore($secondStep)
			);
		}

		return $this->calculateNext($secondStep);
	}

	private function getScore($secondStep)
	{
		$regionScores = [
			'Sul' => 2,
			'Sudeste' => 1,
			'Centro-Oeste' => 3,
			'Nordeste' => 4,
			'Norte' => 5
		];

		$regionScore = $regionScores[$secondStep->getRegion()];

		return $secondStep->getScore() - $regionScore;
	}
}