<?php

namespace App\Validator;

use App\Exception\InvalidBirthday as InvalidBirthdayException;

class Birthday implements ValidatorInterface
{
	public function validate($birthday)
	{
		$this->validateDateFormat($birthday);
		$this->validateDate($birthday);
		$this->validateDateIsLowerThanNow($birthday);
	}

	private function validateDateFormat($date)
	{
		if (!preg_match_all('/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/im', $date)) {
			throw new InvalidBirthdayException('A data de nascimento deve estar no formato DD/MM/AAAA.');
		}
	}

	private function validateDate($date)
	{
		$format = 'd/m/Y';
		$dateTime = \DateTime::createFromFormat($format, $date);

		if ($dateTime->format($format) != $date) {
			throw new InvalidBirthdayException('A data de nascimento informada não é válida.');
		}
	}

	private function validateDateIsLowerThanNow($date)
	{
		$dateTimeNow = new \DateTime('now');
		$dateTime = \DateTime::createFromFormat('d/m/Y', $date);

		if ($dateTime > $dateTimeNow) {
			throw new InvalidBirthdayException('A data de nascimento informada é maior que a data atual.');
		}
	}
}