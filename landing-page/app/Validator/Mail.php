<?php

namespace App\Validator;

use App\Exception\InvalidMail as InvalidMailException;

class Mail implements ValidatorInterface
{
	public function validate($mail)
	{
		if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
			throw new InvalidMailException('O e-mail informado não é válido.');
		}
	}
}