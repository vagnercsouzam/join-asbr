<?php

namespace App\Validator;

use App\Exception\InvalidName as InvalidNameException;

class Name implements ValidatorInterface
{
	public function validate($name)
	{
		$this->validateNumberOfWords($name);
		$this->validateCharacters($name);
	}

	private function validateNumberOfWords($name)
	{
		if (str_word_count($name, 0) < 2) {
			throw new InvalidNameException('O nome deve conter ao menos duas palavras.');
		}
	}

	private function validateCharacters($name)
	{
		if (!preg_match_all('/^[a-záàâãéèêíïóôõöúçñ ]+$/im', $name)) {
			throw new InvalidNameException('O nome não deve conter caracteres especiais.');
		}
	}
}