<?php

namespace App\Validator;

use App\Exception\InvalidPhone as InvalidPhoneException;

class Phone implements ValidatorInterface
{
	public function validate($phone)
	{
		if (!preg_match_all($this->getRegexPattern(), $phone)) {
			throw new InvalidPhoneException('O telefone informado não é válido.');
		}
	}

	private function getRegexPattern()
	{
		return '/^(\([1-9]{2}\) [9]?[0-9]{4}-[0-9]{4})$/im';
	}
}