<?php

namespace App\Validator;

use App\Exception\InvalidRegion as InvalidRegionException;

class Region implements ValidatorInterface
{
	public function validate($region)
	{
		if (!in_array($region, $this->getRegions())) {
			throw new InvalidRegionException('A região informada não existe.');
		}
	}

	private function getRegions()
	{
		return [
			'Sul',
			'Sudeste',
			'Centro-Oeste',
			'Nordeste',
			'Norte'
		];
	}
}