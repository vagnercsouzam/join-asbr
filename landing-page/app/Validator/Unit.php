<?php

namespace App\Validator;

use App\Exception\InvalidUnit as InvalidUnitException;

class Unit implements ValidatorInterface
{
	public function validate($unit)
	{
		if (!in_array($unit, $this->getUnits())) {
			throw new InvalidUnitException('A unidade informada não existe.');
		}
	}

	private function getUnits()
	{
		return [
			'Porto Alegre',
			'Curitiba',
			'São Paulo',
			'Rio de Janeiro',
			'Belo Horizonte',
			'Brasília',
			'Salvador',
			'Recife',
		];
	}
}