<?php

chdir(dirname(__DIR__));

require 'vendor/autoload.php';

$app = new \Phalcon\Mvc\Micro;

$indexController = new \App\Controller\IndexController;
$leadController = new \App\Controller\LeadController;

$app->get('/', [$indexController, 'index']);
$app->get('/lead/units', [$leadController, 'units']);
$app->post('/lead/first-step', [$leadController, 'firstStep']);
$app->post('/lead/second-step', [$leadController, 'secondStep']);

$app->handle();
