var Landing = {
	init: function() {
		Landing.bindEvents();
		Landing.bindMask();
	},
	bindEvents: function() {
		$('button.first-step').click(Landing.validateFirstStep);
		$('button.second-step').click(Landing.validateSecondStep);
		$('select.region').change(Landing.loadUnits);
	},
	bindMask: function() {
		var maskBehavior = function (val) {
		  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		options = {
		  onKeyPress: function(val, e, field, options) {
		      field.mask(maskBehavior.apply({}, arguments), options);
		    }
		};

		$('.phone').mask(maskBehavior, options);
		$('.date').mask('00/00/0000');
	},
	validateFirstStep: function() {
		Landing.request(
			'POST',
			'first-step',
			{
				'name': Landing.getInputValue('nome'),
				'birthday': Landing.getInputValue('data_nascimento'),
				'mail': Landing.getInputValue('email'),
				'phone': Landing.getInputValue('telefone')
			},
			function(data) {
				if (data.valid) {
					Landing.nextStep();
				} else {
					Landing.showError(data.message);
				}
			}
		);
	},
	validateSecondStep: function() {
		Landing.request(
			'POST',
			'second-step',
			{
				'name': Landing.getInputValue('nome'),
				'birthday': Landing.getInputValue('data_nascimento'),
				'mail': Landing.getInputValue('email'),
				'phone': Landing.getInputValue('telefone'),
				'region': $('select.region').val(),
				'unit': $('select.unit').val()
			},
			function(data) {
				if (data.success) {
					Landing.nextStep();
				} else {
					Landing.showError(data.message);
				}
			}
		);
	},
	loadUnits: function() {
		Landing.request(
			'GET',
			'units',
			{
				'region': $('select.region').val()
			},
			function(data) {
				if (data.success) {
					Landing.cleanUnits();
					if (data.units.length == 0) {
						Landing.showError('Desculpe, não existem unidades disponíveis em sua região.');
					} else {
						Landing.appendUnits(data.units);
					}
				} else {
					Landing.showError(data.message);
				}
			}
		);
	},
	cleanUnits: function() {
		$('select.unit').html('<option value="">Selecione a unidade mais próxima</option>');
	},
	appendUnits: function(units) {
		units.forEach(function(unit) {
			$('select.unit').append('<option>' + unit + '</option>');
		});
	},
	nextStep: function() {
		$('.form-step:visible').hide().next().show();
	},
	getInputValue: function(inputName) {
		return $('input[name="' + inputName + '"]').val();
	},
	showError: function(message) {
		alert(message);
	},
	request: function(method, url, data, callback) {
		$.ajax({
			'url': 'lead/' + url,
			'data': data,
			'dataType': 'json',
			'method': method,
			'success': callback
		});
	}
}

$(function() {
	Landing.init();
});