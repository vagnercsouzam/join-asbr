<?php

namespace AppTest\Service\Score;

use PHPUnit_Framework_TestCase;

class AgeCalculatorTest extends PHPUnit_Framework_TestCase
{
	public function testClassExists()
	{
		$this->assertInstanceOf(
			'App\Service\Score\AgeCalculator',
			new \App\Service\Score\AgeCalculator() 
		);
	}

	public function birthdaysAndScoresProvider()
	{
		return [
			['10/01/1906', 5],
			['15/05/2010', 5],
			['20/03/1964', 7],
			['08/09/1950', 7],
			['01/02/1996', 10],
			['27/07/1990', 10],
		];
	}

	/**
     * @dataProvider birthdaysAndScoresProvider
     */
	public function testScoreBasedOnBirthday($birthday, $score)
	{
		$calculator = new \App\Service\Score\AgeCalculator();
		$secondStepEntity = new \App\Entity\SecondStep();
		$secondStepEntity->setBirthday($birthday);

		$this->assertEquals(
			$score,
			$calculator->calculate($secondStepEntity)
		);
	}
}