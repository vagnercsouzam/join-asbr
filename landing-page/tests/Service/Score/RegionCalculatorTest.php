<?php

namespace AppTest\Service\Score;

use PHPUnit_Framework_TestCase;

class RegionCalculatorTest extends PHPUnit_Framework_TestCase
{
	public function testClassExists()
	{
		$this->assertInstanceOf(
			'App\Service\Score\RegionCalculator',
			new \App\Service\Score\RegionCalculator() 
		);
	}

	public function regionAndUnitAndScoreProvider()
	{
		return [
			['Sul', 'Porto Alegre', 8],
			['Sul', 'Curitiba', 8],
			['Sudeste', 'São Paulo', 10],
			['Sudeste', 'Rio de Janeiro', 9],
			['Sudeste', 'Belo Horizonte', 9],
			['Centro-Oeste', 'Brasília', 7],
			['Nordeste', 'Salvador', 6],
			['Nordeste', 'Recife', 6],
			['Norte', '', 5],
		];
	}

	/**
     * @dataProvider regionAndUnitAndScoreProvider
     */
	public function testScoreBasedOnRegionAndUnit($region, $unit, $score)
	{
		$calculator = new \App\Service\Score\RegionCalculator();
		$secondStepEntity = new \App\Entity\SecondStep();
		$secondStepEntity->setRegion($region);
		$secondStepEntity->setUnit($unit);

		$this->assertEquals(
			$score,
			$calculator->calculate($secondStepEntity)
		);
	}
}