<?php

namespace AppTest\Validator;

use PHPUnit_Framework_TestCase;

class BirthdayTest extends PHPUnit_Framework_TestCase
{
	public function testClassExists()
	{
		$this->assertInstanceOf(
			'App\Validator\Birthday',
			new \App\Validator\Birthday() 
		);
	}

	public function validDatesProvider()
	{
		return [
			['09/02/1996'],
			['15/05/2016'],
		];
	}

	/**
     * @dataProvider validDatesProvider
     */
	public function testValidationOfValidDates($birthday)
	{
		(new \App\Validator\Birthday())->validate($birthday);
	}

	public function invalidDatesProvider()
	{
		return [
			['12/12/2017'],
			['20/20/2010'],
			['10/10/10'],
			['20-10-2010'],
		];
	}

	/**
     * @dataProvider invalidDatesProvider
     * @expectedException \App\Exception\InvalidBirthday
     */
	public function testValidationOfInvalidDates($birthday)
	{
		(new \App\Validator\Birthday())->validate($birthday);
	}
}