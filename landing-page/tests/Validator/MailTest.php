<?php

namespace AppTest\Validator;

use PHPUnit_Framework_TestCase;

class MailTest extends PHPUnit_Framework_TestCase
{
	public function testClassExists()
	{
		$this->assertInstanceOf(
			'App\Validator\Mail',
			new \App\Validator\Mail() 
		);
	}

	public function validMailsProvider()
	{
		return [
			['vagnercsouzam@gmail.com'],
			['vagner.souza18@etec.sp.gov.br'],
		];
	}

	/**
     * @dataProvider validMailsProvider
     */
	public function testValidationOfValidMails($mail)
	{
		(new \App\Validator\Mail())->validate($mail);
	}

	public function invalidMailsProvider()
	{
		return [
			['vagner'],
			['gmail.com'],
			['vagner@teste'],
		];
	}

	/**
     * @dataProvider invalidMailsProvider
     * @expectedException \App\Exception\InvalidMail
     */
	public function testValidationOfInvalidMails($mail)
	{
		(new \App\Validator\Mail())->validate($mail);
	}
}