<?php

namespace AppTest\Validator;

use PHPUnit_Framework_TestCase;

class NameTest extends PHPUnit_Framework_TestCase
{
	public function testClassExists()
	{
		$this->assertInstanceOf(
			'App\Validator\Name',
			new \App\Validator\Name() 
		);
	}

	public function validNamesProvider()
	{
		return [
			['Vagner Cavalcante Souza'],
			['Diana Sousa Cavalcante'],
			['Ingrid Amaral de Sousa'],
			['Diana Cavalcante'],
			['João Nascimento'],
			['Antônio Silva'],
			['André Aparecido']
		];
	}

	/**
     * @dataProvider validNamesProvider
     */
	public function testValidationOfValidNames($name)
	{
		(new \App\Validator\Name())->validate($name);
	}

	public function invalidNamesProvider()
	{
		return [
			['Diana'],
			['Vagner'],
			['_Vagner Cavalcante'],
			['Vagner-Cavalcante'],
		];
	}

	/**
     * @dataProvider invalidNamesProvider
     * @expectedException \App\Exception\InvalidName
     */
	public function testValidationOfInvalidNames($name)
	{
		(new \App\Validator\Name())->validate($name);
	}
}