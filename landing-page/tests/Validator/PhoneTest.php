<?php

namespace AppTest\Validator;

use PHPUnit_Framework_TestCase;

class PhoneTest extends PHPUnit_Framework_TestCase
{
	public function testClassExists()
	{
		$this->assertInstanceOf(
			'App\Validator\Phone',
			new \App\Validator\Phone() 
		);
	}

	public function validPhonesProvider()
	{
		return [
			['(11) 96611-9106'],
			['(11) 4488-0700'],
		];
	}

	/**
     * @dataProvider validPhonesProvider
     */
	public function testValidationOfValidPhones($phone)
	{
		(new \App\Validator\Phone())->validate($phone);
	}

	public function invalidPhonesProvider()
	{
		return [
			['(11) 0000-00'],
			['(13) 00-0000'],
			['(21) 0-0'],
		];
	}

	/**
     * @dataProvider invalidPhonesProvider
     * @expectedException \App\Exception\InvalidPhone
     */
	public function testValidationOfInvalidPhones($phone)
	{
		(new \App\Validator\Phone())->validate($phone);
	}
}