<?php

namespace AppTest\Validator;

use PHPUnit_Framework_TestCase;

class RegionTest extends PHPUnit_Framework_TestCase
{
	public function testClassExists()
	{
		$this->assertInstanceOf(
			'App\Validator\Region',
			new \App\Validator\Region() 
		);
	}

	public function validRegionsProvider()
	{
		return [
			['Sul'],
			['Sudeste'],
			['Centro-Oeste'],
			['Nordeste'],
			['Norte']
		];
	}

	/**
     * @dataProvider validRegionsProvider
     */
	public function testValidationOfValidRegions($region)
	{
		(new \App\Validator\Region())->validate($region);
	}

	public function invalidRegionsProvider()
	{
		return [
			['São Paulo'],
			['Rio de Janeiro'],
			['Curitiba'],
		];
	}

	/**
     * @dataProvider invalidRegionsProvider
     * @expectedException \App\Exception\InvalidRegion
     */
	public function testValidationOfInvalidRegions($region)
	{
		(new \App\Validator\Region())->validate($region);
	}
}