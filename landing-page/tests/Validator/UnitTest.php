<?php

namespace AppTest\Validator;

use PHPUnit_Framework_TestCase;

class UnitTest extends PHPUnit_Framework_TestCase
{
	public function testClassExists()
	{
		$this->assertInstanceOf(
			'App\Validator\Unit',
			new \App\Validator\Unit() 
		);
	}

	public function validUnitsProvider()
	{
		return [
			['Porto Alegre'],
			['Curitiba'],
			['São Paulo'],
			['Rio de Janeiro'],
			['Belo Horizonte'],
			['Brasília'],
			['Salvador'],
			['Recife'],
		];
	}

	/**
     * @dataProvider validUnitsProvider
     */
	public function testValidationOfValidUnits($unit)
	{
		(new \App\Validator\Unit())->validate($unit);
	}

	public function invalidUnitsProvider()
	{
		return [
			['Barueri'],
			['Campinas'],
			['Paranapiacaba'],
		];
	}

	/**
     * @dataProvider invalidUnitsProvider
     * @expectedException \App\Exception\InvalidUnit
     */
	public function testValidationOfInvalidUnits($unit)
	{
		(new \App\Validator\Unit())->validate($unit);
	}
}